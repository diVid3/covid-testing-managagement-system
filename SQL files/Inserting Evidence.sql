USE [Covid19ManagementSystem]
GO

INSERT INTO [dbo].[Evidence]
           ([Name]
           ,[CommonName]
           ,[ApiId])
     VALUES

	 ('Community transmission country','Community transmission country','p_5'),
	 ('Caring for COVID-19 suspected person without protection','Caring for COVID-19 suspected person without protection','p_25'),
	 ('Direct physical contact with a person suspected of having COVID-19','Direct physical contact with a person suspected of having COVID-19','p_26'),
	 ('Prolonged direct contact with a person suspected of having COVID-19','Prolonged direct contact with a person suspected of having COVID-19','p_27'),
	 ('No significant contact with an infected person','No significant contact with an infected person','p_11'),
	 ('No contact with an infected person','No contact with an infected person','p_15'),
	 ('Current cancer','Current cancer','p_18'),
	 ('Diseases or drugs that weaken immune system','Diseases or drugs that weaken immune system','p_19'),
	 ('Obesity','Obesity','p_24'),
	 ('Long-term stay at a care facility or nursing home','Long-term stay at a care facility or nursing home','p_22'),
	 ('Diabetes','Diabetes','p_23'),
	 ('Cardiovascular disease','Cardiovascular disease','p_17'),
	 ('History of chronic lung disease','History of chronic lung disease','p_16'),
	 ('History of chronic liver disease','History of chronic liver disease','p_20'),
	 ('History of chronic kidney disease','History of chronic kidney disease','p_21'), -- end of risk factors 
	 ('Fever','Fever','s_0'),
	 ('Cough','Cough','s_1'),
	 ('Shortness of breath','Shortness of breath','s_2'),
	 ('Unconsciousness','Unconsciousness','s_10'),
	 ('Not responding normally','Not responding normally','s_11'),
	 ('Symptoms quickly worsening','Symptoms quickly worsening','s_12'),
	 ('Fever between 37.5�C and 38�C (99.5�F and 100.4�F)','Fever between 37.5�C and 38�C (99.5�F and 100.4�F)','s_22'),
	 ('Fever between 38�C and 40�C (100.4�F and 104�F)','Fever between 38�C and 40�C (100.4�F and 104�F)','s_23'),
	 ('Fever over 40�C (104�F)','Fever over 40�C (104�F)','s_4'),
	 ('Fever not measured','Fever not measured','s_5'),
	 ('Rapid breathing','Rapid breathing','s_13'),
	 ('Coughing up blood','Coughing up blood','s_14'),
	 ('Fatigue','Fatigue','s_15'),
	 ('Muscle pain','Muscle pain','s_16'),
	 ('Chills','Chills','s_17'),
	 ('Headache','Headache','s_18'),
	 ('Diarrhea','Diarrhea','s_19'),
	 ('Nausea','Nausea','s_20'),
	 ('Sore throat','Sore throat','s_21'),
	 ('Impaired taste or smell','Impaired taste or smell','s_24')

           
GO
SELECT * FROM Evidence