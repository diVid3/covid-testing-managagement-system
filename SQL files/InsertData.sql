-- TODO: Make idempotent
USE Covid19ManagementSystem 
GO

INSERT INTO [Country] ([Abbreviation], [Name])
VALUES
('ZA', 'TODO'),
('US', 'TODO'),
('FR', 'TODO'),
('UK', 'TODO'),
('NZ', 'TODO'),
('AU', 'TODO'),
('EG', 'TODO'),
('IL', 'TODO'),
('BR', 'TODO');

INSERT INTO [Speciality] ([Name])
VALUES
('GP'),
('Physician'),
('Nurse'),
('Assistant'),
('Admin'),
('Aide'),
('Caretaker'),
('Technician'),
('Pharmacist'),
('Hygienist'),
('Therapist'),
('Pediatrician'),
('Surgeon'),
('SuperUser')

INSERT INTO [Role] ([Name], [NormalizedName])
VALUES
('Admin', 'ADMIN'),
('Doctor', 'DOCTOR')

INSERT INTO [PersonType] ([Name])
VALUES
('Patient'),
('Professional')
GO
