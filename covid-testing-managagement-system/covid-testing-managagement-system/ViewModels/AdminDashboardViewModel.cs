﻿using System.Collections.Generic;

namespace covid_testing_managagement_system.Models
{
    public class AdminDashboardViewModel
    {
        public List<Professional> Professionals { get; set; }
        public bool IsNewDoctors { get; set; }

    }
}
