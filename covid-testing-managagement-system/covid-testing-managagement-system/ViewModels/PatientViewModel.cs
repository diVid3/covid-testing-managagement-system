﻿using System.ComponentModel.DataAnnotations;

namespace covid_testing_managagement_system.Models
{
    public class PatientViewModel
    {
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]
        [StringLength(50)]
        public string Email { get; set; }
        [Required]
        [StringLength(20)]
        public string IdNumber { get; set; }
    }
}
