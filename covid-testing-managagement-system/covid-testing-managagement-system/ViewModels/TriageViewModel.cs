﻿using covid_testing_managagement_system.Models.Infermedica.Request;
using covid_testing_managagement_system.Models.Infermedica.Response;

namespace covid_testing_managagement_system.ViewModels
{
    public class TriageViewModel
    {
        public InfermedicaRequest Request { get; set; }
        public TriageResponse Response { get; set; }
    }
}
