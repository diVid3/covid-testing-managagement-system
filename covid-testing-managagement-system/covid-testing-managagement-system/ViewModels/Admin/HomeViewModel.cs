using covid_testing_managagement_system.Models;

namespace covid_testing_managagement_system.ViewModels.Admin
{
    public class HomeViewModel
    {
        public Professional professional { get; set; }
        public bool isAdmin { get; set; }
    }
}