﻿using covid_testing_managagement_system.Interfaces;
using covid_testing_managagement_system.Models.Admin;
using Dapper;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.Repositories
{
    public class InterviewRepository : IInterviewRepository
    {

        private readonly string ConnectionString;


        public InterviewRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public SqlConnection getConn()
        {
            return new SqlConnection(ConnectionString);
        }


        public async Task<int> AddInterviewAsync(int PatientId)
        {
            int interviewId = -1;
            try
            {
                string Command = "INSERT INTO[dbo].[Interview] ([PatientId],[Reviewed] ,[Outcome] ,[ProfessionalId]) VALUES (@PatientId,@Review,@OutCome,Null) SELECT SCOPE_IDENTITY()";
                   interviewId = await getConn().ExecuteScalarAsync<int>(Command, new { PatientId = PatientId, @Review = false, OutCome = false });
            }
            catch(Exception e){
                string DeletePatient = @"DELETE FROM [dbo].[Patient] WHERE Id = @UserId ";
                string DeletePersonData = @"DELETE FROM [dbo].[PersonalInfo] WHERE Id = @UserId ";
                using (SqlConnection conn = getConn()) {

                    if (conn.ExecuteScalar<int>("SELECT COUNT(Id) FROM PersonalInfo WHERE Id = @UserId", new { UserId = PatientId }) > 0)
                    {
                        using (SqlTransaction tran = conn.BeginTransaction())
                        {
                            await conn.ExecuteAsync(DeletePatient, new { UserId = PatientId }, transaction: tran);
                            await conn.ExecuteAsync(DeletePersonData, new { UserId = PatientId }, transaction: tran);
                            tran.Commit();
                        }
                    }
                }
            }
            return interviewId;
        }

        public async Task<Interview> EditInterview(Interview interview)
        {
            String Command = "UPDATE[dbo].[Interview] SET[PatientId] = @PatientId ,[Reviewed] = @Review ,[Outcome] = @OutCome ,[ProfessionalId] = @ProfId WHERE Interview.Id = @Id";

            await getConn().ExecuteAsync(Command, new { PatientId = interview.PatientId, Review = interview.Reviewed, OutCome = interview.Outcome, ProfId = interview.ReviewerDoctorId, Id = interview.Id });


            return await GetInterview(interview.Id);
        }

        public async Task<Interview> GetInterview(int id)
        {

            return (await getConn().QueryAsync<Interview>("SELECT [Id],[PatientId],[Reviewed],[Outcome],[ProfessionalId] FROM[dbo].[Interview] WHERE [Id] = @ID", new { ID = id })).FirstOrDefault<Interview>();
        }


        public async Task<IEnumerable<Interview>> GetInterviewsAsync()
        {
            return await getConn().QueryAsync<Interview>("SELECT [Id],[PatientId],[Reviewed],[Outcome],[ProfessionalId] FROM[dbo].[Interview]");
        }

        public async Task<IEnumerable<Interview>> GetInterviewsByReviewedStatusAsync(bool reviewed)
        {
            return await getConn().QueryAsync<Interview>("SELECT [Id],[PatientId],[Reviewed],[Outcome],[ProfessionalId] FROM[dbo].[Interview] WHERE [Reviewed] = @Reviewed", new { Reviewed = reviewed });
        }
    }
}
