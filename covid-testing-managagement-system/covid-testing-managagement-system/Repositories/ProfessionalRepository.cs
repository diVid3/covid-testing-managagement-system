﻿using covid_testing_managagement_system.Models;
using covid_testing_managagement_system.RepositoryInterfaces;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace covid_testing_managagement_system.Repositories
{
    public class ProfessionalRepository : IProfessionalRepository
    {

        private readonly string ConnectionString;
        private readonly ILogger<ProfessionalRepository> _logger;

        public ProfessionalRepository(ILogger<ProfessionalRepository> logger, IConfiguration configuration)
        {
            _logger = logger;
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public SqlConnection getConn()
        {
            return new SqlConnection(ConnectionString);
        }

        public async Task<Professional> AddProfessionalAsync(Professional professional, SqlConnection callerConnection = null)
        {
            _logger.LogDebug($"Creating new Professional: {professional.Email}");
            using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                SqlConnection conn = null;
        
                if (callerConnection == null)
                {
                    conn = getConn();
                    conn.Open();
                }
                else
                {
                    conn = callerConnection;
                }
        
                string insertPersonalInfo = @"INSERT INTO[dbo].[PersonalInfo] ([FirstName] ,[LastName] ,[NormalizedEmail] ,[IdNumber], [PersonTypeId])
                                    VALUES (@FirstName,@LastName,@EmailAddress,@IdNumber,
                                    (SELECT [Id] FROM [PersonType] WHERE [Name] = 'Professional')) SELECT SCOPE_IDENTITY()";
                int insertedId = (await conn.ExecuteScalarAsync<int>(insertPersonalInfo, new
                {
                    FirstName = professional.FirstName,
                    LastName = professional.LastName,
                    EmailAddress = professional.Email.ToUpperInvariant(),
                    Idnumber = professional.IdNumber
                }));

                int countryId = await conn.QueryFirstAsync<int>($"SELECT [Id] FROM [Country] WHERE [Abbreviation] = @{nameof(professional.Country)}",
                    new { Country = professional.Country.ToString() });

                int specialityId = await conn.QuerySingleAsync<int>($"SELECT [Id] FROM [Speciality] WHERE [Name] = @{nameof(professional.Speciality)}",
                    new { Speciality = professional.Speciality.ToString() });

                string insertProfessional = @"INSERT INTO [dbo].[Professional] ([Id],[RegistrationNumber],[Country],[Speciality],[Reviewed])
                                    VALUES (@Id, @RegNum, @Country, @Speciality, @Review)";

                await conn.ExecuteAsync(insertProfessional, new
                {
                    Id = insertedId,
                    RegNum = professional.RegistrationNumber,
                    Country = countryId,
                    Speciality = specialityId,
                    @Review = false
                });
                professional.Id = insertedId;
                
                // Only close the connection if it is not managed by the caller
                if (callerConnection == null)
                {
                    conn.Close();
                }
                transaction.Complete();
                _logger.LogDebug($"Successfully added new professional: {professional.Email}");
            }
            return professional;
        }

        public async Task<Professional> GetProfessionalByEmailAsync(string email)
        {
            return (await getConn().QueryAsync<Professional>("SELECT Professional.Id, Professional.Country , Professional.RegistrationNumber,Professional.Speciality, Professional.Reviewed, [PersonalInfo].FirstName, [PersonalInfo].LastName, [PersonalInfo].NormalizedEmail, [PersonalInfo].IdNumber FROM Professional INNER JOIN [PersonalInfo] ON Professional.Id = [PersonalInfo].Id WHERE [PersonalInfo].NormalizedEmail = @NormalizedEmail", new { NormalizedEmail = email.ToUpperInvariant() })).FirstOrDefault();
        }

        public async Task<Professional> GetProfessionalByIdAsync(int id)
        {
            return (await getConn().QueryAsync<Professional>("SELECT Professional.Id, Professional.Country , Professional.RegistrationNumber,Professional.Speciality, Professional.Reviewed, [PersonalInfo].FirstName, [PersonalInfo].LastName, [PersonalInfo].NormalizedEmail, [PersonalInfo].IdNumber FROM Professional INNER JOIN [PersonalInfo] ON Professional.Id = [PersonalInfo].Id WHERE Professional.Id = @Id", new { Id = id })).FirstOrDefault();
        }

        public async Task<Professional> GetProfessionalByIdNumberAsync(string IdNumber)
        {
            return (await getConn().QueryAsync<Professional>("SELECT Professional.Id, Professional.Country , Professional.RegistrationNumber,Professional.Speciality, Professional.Reviewed, [PersonalInfo].FirstName, [PersonalInfo].LastName, [PersonalInfo].NormalizedEmail, [PersonalInfo].IdNumber FROM Professional INNER JOIN [PersonalInfo] ON Professional.Id = [PersonalInfo].Id WHERE [PersonalInfo].IdNumber = @IDNumber", new { IDNumber = IdNumber })).FirstOrDefault();
        }

        public async Task<Professional> GetProfessionalByRegistrationNumberAsync(int registartionNumber)
        {
            return (await getConn().QueryAsync<Professional>("SELECT Professional.Id, Professional.Country , Professional.RegistrationNumber,Professional.Speciality, Professional.Reviewed, [PersonalInfo].FirstName, [PersonalInfo].LastName, [PersonalInfo].NormalizedEmail, [PersonalInfo].IdNumber FROM Professional INNER JOIN [PersonalInfo] ON Professional.Id = [PersonalInfo].Id WHERE Professional.RegistrationNumber = @RegistrationNumber", new { RegistrationNumber = registartionNumber })).FirstOrDefault();
        }

        public async Task<IEnumerable<Professional>> GetProfessionalsAsync()
        {

            return await getConn().QueryAsync<Professional>("SELECT Professional.Id, Professional.Country , Professional.RegistrationNumber,Professional.Speciality, Professional.Reviewed, [PersonalInfo].FirstName, [PersonalInfo].LastName, [PersonalInfo].NormalizedEmail, [PersonalInfo].IdNumber FROM Professional INNER JOIN [PersonalInfo] ON Professional.Id = [PersonalInfo].Id ");
        }

        public async Task<IEnumerable<Professional>> GetProfessionalssByReviewedStatusAsync(bool reviewed)
        {
            return await getConn().QueryAsync<Professional>("SELECT Professional.Id, Professional.Country, Professional.RegistrationNumber, Professional.Speciality, Professional.Reviewed, [PersonalInfo].FirstName, [PersonalInfo].LastName, [PersonalInfo].NormalizedEmail, [PersonalInfo].IdNumber FROM Professional INNER JOIN [PersonalInfo] ON Professional.Id = [PersonalInfo].Id  WHERE [Reviewed] = @Reviewed", new { Reviewed = reviewed });
        }

        public async Task UpdateProfessionalReviewedStatusAsync(int id, int reviewedStatus)
        {
            await getConn().QueryAsync<Professional>("UPDATE Professional SET [Reviewed] = @reviewedStatus WHERE [Id] = @id", new { reviewedStatus, id });
        }

        public async Task<int> GetUserIdFromProfessionalId(string ProfessionalId)
        {
            return await getConn().ExecuteScalarAsync<int>("SELECT TOP(1) [User].Id FROM PersonalInfo INNER JOIN [User] ON PersonalInfo.NormalizedEmail = [User].NormalizedEmail WHERE PersonalInfo.Id = @ProfId",new {ProfId = ProfessionalId });
        }
    }
}
