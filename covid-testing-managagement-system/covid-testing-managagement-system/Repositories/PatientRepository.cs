﻿using covid_testing_managagement_system.Models;
using covid_testing_managagement_system.RepositoryInterfaces;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.Repositories
{
    public class PatientRepository : IPatientRepository
    {

        private readonly string ConnectionString;


        public PatientRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public SqlConnection getConn()
        {
            return new SqlConnection(ConnectionString);
        }

        public async Task<Patient> AddPatientAsync(Patient patient)
        {
            SqlConnection conn = getConn();
            int insertedId;
            conn.Open();
            using (SqlTransaction trans = conn.BeginTransaction())
            {
                string InsertUser = "INSERT INTO[dbo].[PersonalInfo] ([FirstName] ,[LastName] ,[NormalizedEmail] ,[IdNumber],[PersonTypeId])VALUES (@FirstName,@LastName,@EmailAddress,@IdNumber,(SELECT TOP(1) Id FROM PersonType WHERE [Name] = 'Patient')) SELECT SCOPE_IDENTITY()";
                insertedId = (await conn.ExecuteScalarAsync<int>(InsertUser, new { FirstName = patient.FirstName, LastName = patient.LastName, EmailAddress = patient.Email, Idnumber = patient.IdNumber }, transaction: trans));

                string InsertPatient = "INSERT INTO [dbo].[Patient] ([Id] ,[Age] ,[Sex]) VALUES (@Id,@Age ,@Sex)";

                await conn.ExecuteAsync(InsertPatient, new { Id = insertedId, Age = patient.Age, Sex = patient.Sex }, transaction: trans);
                trans.Commit();
            }
            return await GetPatientByIdAsync(insertedId);
        }

        public async Task<Patient> GetPatientByIdAsync(int id)
        {
            return (await getConn().QueryAsync<Patient>("SELECT Patient.Id, Patient.Age, Patient.Sex, [PersonalInfo].FirstName, [PersonalInfo].LastName, [PersonalInfo].NormalizedEmail, [PersonalInfo].IdNumber FROM Patient INNER JOIN [PersonalInfo] ON Patient.Id = [PersonalInfo].Id WHERE Patient.Id = @PatientId", new { PatientId = id })).FirstOrDefault<Patient>();
        }
    }
}
