﻿using covid_testing_managagement_system.Models;
using covid_testing_managagement_system.RepositoryInterfaces;
using covid_testing_managagement_system.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Data.SqlClient;
using System.Diagnostics;

namespace covid_testing_managagement_system.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration configuration;
        private readonly string ConnectionString;
        private readonly IEmailSender _emailSender;
        private readonly IProfessionalRepository _professionalRepository;

        public HomeController(ILogger<HomeController> logger,IConfiguration iConfig, IEmailSender emailSender,
        IProfessionalRepository professionalRepository)
        {
            _logger = logger;
            _professionalRepository = professionalRepository;
            configuration = iConfig;
            ConnectionString = configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            this._emailSender = emailSender;
        }

        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
