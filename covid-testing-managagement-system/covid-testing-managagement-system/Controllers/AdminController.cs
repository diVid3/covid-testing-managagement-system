using covid_testing_managagement_system.Interfaces;
using covid_testing_managagement_system.Models;
using covid_testing_managagement_system.Models.Admin;
using covid_testing_managagement_system.RepositoryInterfaces;
using covid_testing_managagement_system.Services;
using covid_testing_managagement_system.ViewModels.Admin;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Security.Claims;

namespace covid_testing_managagement_system.Controllers
{
    public class AdminController : Controller
    {
        private readonly ILogger<AdminController> _logger;

        private readonly IPatientRepository _patientRepository;
        private readonly IProfessionalRepository _professionalRepository;
        private readonly IInterviewEvidenceRepository _interviewEvidenceRepository;
        private readonly IInterviewRepository _interviewRepository;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly string _connectionString;
        private readonly IEmailSender _emailSender;

        public AdminController(ILogger<AdminController> logger,
        IPatientRepository patientRepository,
        IProfessionalRepository professionalRepository,
        IInterviewEvidenceRepository interviewEvidenceRepository,
        IInterviewRepository interviewRepository,
        UserManager<User> userManager,
        SignInManager<User> signInManager,
        IConfiguration configuration,
        IEmailSender emailSender)
        {
            _logger = logger;
            _signInManager = signInManager;
            _userManager = userManager;
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _patientRepository = patientRepository;
            _professionalRepository = professionalRepository;
            _interviewEvidenceRepository = interviewEvidenceRepository;
            _interviewRepository = interviewRepository;
            _emailSender = emailSender;
        }

        [Authorize(Roles = "Doctor")]
        [HttpGet]
        public async Task<IActionResult> PatientReport(int Id)
        {
            PatientReportViewModel report = new PatientReportViewModel();

            report.interview = await _interviewRepository.GetInterview(Id);

            if (report.interview == null)
            {
                return NotFound();
            }

            report.patient = await _patientRepository.GetPatientByIdAsync(report.interview.PatientId);
            List<InterviewEvidence> unsortedEvidence = (await _interviewEvidenceRepository.GetInterviewEvidenceAsync(Id)).ToList();

            Regex symptomRegex = new Regex(@"^s_*");

            IEnumerable<InterviewEvidence> riskFactorsAbsent = from evidence in unsortedEvidence
                                                               where evidence.Choice == false && !symptomRegex.IsMatch(evidence.ApiId)
                                                               select evidence;

            IEnumerable<InterviewEvidence> riskFactorsPresent = from evidence in unsortedEvidence
                                                                where evidence.Choice == true && !symptomRegex.IsMatch(evidence.ApiId)
                                                                select evidence;

            IEnumerable<InterviewEvidence> symptomsAbsent = from evidence in unsortedEvidence
                                                            where evidence.Choice == false && symptomRegex.IsMatch(evidence.ApiId)
                                                            select evidence;

            IEnumerable<InterviewEvidence> symptomsPresent = from evidence in unsortedEvidence
                                                             where evidence.Choice == true && symptomRegex.IsMatch(evidence.ApiId)
                                                             select evidence;

            report.RiskFactorsAbsent = riskFactorsAbsent.ToList();
            report.RiskFactorsPresent = riskFactorsPresent.ToList();
            report.SymptomsAbsent = symptomsAbsent.ToList();
            report.SymptomsPresent = symptomsPresent.ToList();

            return View(report);
        }

        [Authorize(Roles = "Doctor")]
        [HttpPost]
        public async Task<IActionResult> RecommendPatient(int Id)
        {

            Interview interview = await _interviewRepository.GetInterview(Id);
            interview.Outcome = true;
            interview.Reviewed = true;
            interview.ReviewerDoctorId = (await _professionalRepository.GetProfessionalByEmailAsync(User.Identity.Name)).Id;
            await _interviewRepository.EditInterview(interview);
            _emailSender.SendTestingRecommendationEmail((await _patientRepository.GetPatientByIdAsync(interview.PatientId)).NormalizedEmail);
            return RedirectToAction(nameof(PatientReport), new { Id });
        }

        [Authorize(Roles = "Doctor")]
        [HttpPost]
        public async Task<IActionResult> RejectPatient(int Id)
        {
            Interview interview = await _interviewRepository.GetInterview(Id);
            interview.Outcome = false;
            interview.Reviewed = true;
            interview.ReviewerDoctorId = (await _professionalRepository.GetProfessionalByEmailAsync(User.Identity.Name)).Id;
            await _interviewRepository.EditInterview(interview);
            _emailSender.SendTestingRejectionEmail((await _patientRepository.GetPatientByIdAsync(interview.PatientId)).NormalizedEmail);
            return RedirectToAction(nameof(PatientReport), new { Id });
        }

        [HttpGet]
        public IActionResult GetNewCases()
        {
            return RedirectToAction(nameof(Dashboard), new { view = true });
        }

        [HttpGet]
        public IActionResult GetOldCases()
        {
            return RedirectToAction(nameof(Dashboard), new { view = false });
        }


        public async Task<IActionResult> Index()
        {
            return RedirectToAction(nameof(AdminController.Dashboard));
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Dashboard(bool view = true)
        {
            User currentUser = await _userManager.FindByEmailAsync(User.Identity.Name);
            IList<string> roles = await _userManager.GetRolesAsync(currentUser);

            if (roles.Contains("Admin"))
            {
                IEnumerable<Professional> professionals;
                professionals = await _professionalRepository.GetProfessionalssByReviewedStatusAsync(!view);
                AdminDashboardViewModel adminModel = new AdminDashboardViewModel
                {
                    Professionals = professionals.ToList(),
                    IsNewDoctors = view
                };
                return View("AdminDashboard", adminModel);
            }
            else if (roles.Contains("Doctor"))
            {
                IEnumerable<Interview> interviews;
                IEnumerable<Patient> patients;
                interviews = await _interviewRepository.GetInterviewsByReviewedStatusAsync(!view);
                patients = interviews.Select(inter => _patientRepository.GetPatientByIdAsync(inter.PatientId).Result);

                Professional pro = await _professionalRepository.GetProfessionalByEmailAsync(currentUser.NormalizedEmail);
                string proName = $"{pro.FirstName} {pro.LastName}";

                DoctorDashboardViewModel doctorModel = new DoctorDashboardViewModel
                {
                    Interviews = interviews.ToList(),
                    Patients = patients.ToList(),
                    ProfessionalName = proName,
                    IsNewPatients = view
                };
                return View("DoctorDashboard", doctorModel);
            }
            else
            {
                return RedirectToAction(nameof(AdminController.PendingApproval));
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> GrantDoctorAccess(string id, bool view = true)
        {

            User user = await _userManager.FindByIdAsync((await _professionalRepository.GetUserIdFromProfessionalId(id)).ToString());

            if (await _userManager.IsInRoleAsync(user, "Doctor"))
            {
                TempData["ErrorMessage"] = $"{user.NormalizedEmail} is already in the Doctor role";
                return RedirectToAction(nameof(Dashboard), new { view });
            }

            IdentityResult addResult = await _userManager.AddToRoleAsync(user, "Doctor");
            if (!addResult.Succeeded)
            {
                string errorDescription = addResult.Errors.FirstOrDefault().Description;
                _logger.LogError($"{errorDescription}");
                TempData["ErrorMessage"] = errorDescription;
                return RedirectToAction(nameof(Dashboard), new { view });
            }

            try
            {
                await _professionalRepository.UpdateProfessionalReviewedStatusAsync(int.Parse(id), 1);
            }
            catch (Exception)
            {
                TempData["ErrorMessage"] = $"{user.NormalizedEmail} failed to update doctor in database";
                return RedirectToAction(nameof(Dashboard), new { view });
            }

            _emailSender.SendRegistrationApprovedEmail(user.NormalizedEmail);
            TempData["SuccessMessage"] = $"{user.NormalizedEmail} successfully granted medical professional role";
            return RedirectToAction(nameof(Dashboard), new { view });
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> RejectDoctorAccess(string id, bool view)
        {
            User user = await _userManager.FindByIdAsync((await _professionalRepository.GetUserIdFromProfessionalId(id)).ToString());

            try
            {
                await _professionalRepository.UpdateProfessionalReviewedStatusAsync(int.Parse(id), 1);
            }
            catch (Exception)
            {
                TempData["ErrorMessage"] = $"{user.NormalizedEmail} failed to update doctor in database";
                return RedirectToAction(nameof(Dashboard), new { view });
            }

            TempData["SuccessMessage"] = $"{user.NormalizedEmail} successfully rejected from medical professional role";
            _emailSender.SendRegistrationRejectedEmail(user.NormalizedEmail);
            return RedirectToAction(nameof(Dashboard), new { view = true });
        }

        [Authorize(Roles = "Admin, Doctor")]
        [HttpGet]
        public async Task<IActionResult> Home()
        {
            User currentUser = await _userManager.FindByEmailAsync(User.Identity.Name);
            bool isAdmin = await _userManager.IsInRoleAsync(currentUser, "Admin");

            Professional pro = null;

            if (!isAdmin) {
                pro = await _professionalRepository.GetProfessionalByEmailAsync(User.Identity.Name);
            }

            HomeViewModel homeViewModel = new HomeViewModel();
            homeViewModel.professional = pro;
            homeViewModel.isAdmin = isAdmin;
            return View(homeViewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            ClaimsPrincipal currentUser = this.User;
            if (currentUser.Identity.IsAuthenticated)
            {
                return RedirectToAction(nameof(AdminController.Dashboard));
            }
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                    return RedirectToAction(nameof(AdminController.Dashboard));
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToAction(nameof(Lockout));
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> PendingApproval()
        {
            Professional professional = await _professionalRepository.GetProfessionalByEmailAsync(User.Identity.Name);
            User currentUser = await _userManager.FindByEmailAsync(User.Identity.Name);

            if (await _userManager.IsInRoleAsync(currentUser, "Admin") || await _userManager.IsInRoleAsync(currentUser, "Doctor"))
            {
                return RedirectToAction(nameof(AdminController.Dashboard));
            }

            if (professional == null)
            {
                return RedirectToAction(nameof(AdminController.Dashboard));
            }

            if (professional.Reviewed)
            {
                ViewData["message"] = "Unfortunately your application has been rejected.";
            }
            else
            {
                ViewData["message"] = "You will be notified by email when your application has been reviewed.";
            }
            
            await _signInManager.SignOutAsync();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterAsync(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                Professional prof = model.Professional;
                prof.Email = model.Email;
                prof.IdNumber = "TODO";
                var user = new User { NormalizedEmail = model.Email.ToUpperInvariant() };
                _logger.LogInformation($"Attempting to register new professional: {prof.Email} ...");

                // These will be executed within the Transaction in UserStore.CreateAsync
                user.AddCommand((connection) =>
                {
                    // Needs to be synchronous to avoid executing after the connection has been closed
                    _professionalRepository.AddProfessionalAsync(prof, connection).Wait();
                });

                IdentityResult result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation($"Created a new user: {user.NormalizedEmail}");
                    await _signInManager.SignInAsync(user, isPersistent: false);

                    return RedirectToAction(nameof(AdminController.PendingApproval));
                }
                else
                {
                    _logger.LogInformation($"Failed to create a user: {user.NormalizedEmail}");
                    _logger.LogInformation(result.ToString());
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out");
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion

    }
}