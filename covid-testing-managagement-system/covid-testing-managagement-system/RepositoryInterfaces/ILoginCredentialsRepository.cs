﻿using covid_testing_managagement_system.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.RepositoryInterfaces
{
    public interface ILoginCredentialsRepository
    {
        public Task<LoginCredentials> GetLoginByProfessionalAsync(Professional professional);
        public Task<IEnumerable<LoginCredentials>> GetLoginsByApprovalStatusAsync(bool isApproved);
        public Task<bool> AddLoginAsync(LoginCredentials login);
    }
}
