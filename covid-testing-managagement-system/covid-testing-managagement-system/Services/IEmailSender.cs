using System.Threading.Tasks;

namespace covid_testing_managagement_system.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
        void SendTestingRecommendationEmail(string targetEmail);
        void SendTestingRejectionEmail(string targetEmail);
        void SendRegistrationApprovedEmail(string targetEmail);
        void SendRegistrationRejectedEmail(string targetEmail);
    }
}