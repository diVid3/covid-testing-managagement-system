using Microsoft.Extensions.Configuration;

namespace covid_testing_managagement_system.Services
{
    public class EmailCredentials : IEmailCredentials
    {
        public EmailCredentials(IConfiguration configuration)
        {
            configuration.GetSection("EmailCredentials").Bind(this);
        }

        // You'll need to set these user secrets below in order for the
        // emailing service to work. You can use the dotnet cli to do this:
        //
        // dotnet user-secrets set "EmailCredentials:MailServer" "smtp.gmail.com"
        // dotnet user-secrets set "EmailCredentials:MailPort" "465"
        // dotnet user-secrets set "EmailCredentials:SenderName" "Bob"
        // dotnet user-secrets set "EmailCredentials:SenderAddress" "11smtptest11@gmail.com"
        // dotnet user-secrets set "EmailCredentials:Password" "totallyLegitPass"
        //
        // You can also remove secrets by doing:
        //
        // dotnet user-secrets remove "EmailCredentials:SenderAddress"
        //
        // They are stored at:
        //
        // %APPDATA%\Microsoft\UserSecrets\<user_secrets_id>\secrets.json   (Windows)
        // ~/.microsoft/usersecrets/<user_secrets_id>/secrets.json          (Mac / Linux)
        //
        // Your user_secrets_id can be found in the .csproj file.

        public string MailServer { get; set; }
        public int MailPort { get; set; }
        public string SenderName { get; set; }
        public string SenderAddress { get; set; }
        public string Password { get; set; }
    }
}