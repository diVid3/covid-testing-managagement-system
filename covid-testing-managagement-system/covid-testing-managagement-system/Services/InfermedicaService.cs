﻿using covid_testing_managagement_system.Models.Infermedica;
using covid_testing_managagement_system.Models.Infermedica.Request;
using covid_testing_managagement_system.Models.Infermedica.Response;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.Services
{
    public class InfermedicaService : IInfermedicaService
    {
        private const string DIAGNONSIS_ENDPOINT = "https://api.infermedica.com/covid19/diagnosis";
        private const string TRIAGE_ENDPOINT = "https://api.infermedica.com/covid19/triage";
        private readonly IHttpClientFactory clientFactory;
        private readonly IInfermedicaCredentials credentials;
        private readonly ILogger<InfermedicaService> logger;

        public InfermedicaService(ILogger<InfermedicaService> logger, IInfermedicaCredentials credentials, IHttpClientFactory clientFactory)
        {
            this.logger = logger;
            this.clientFactory = clientFactory;
            this.credentials = credentials;
        }

        public async Task<DiagnonsisResponse> SendDiagnosisRequestAsync(InfermedicaRequest diagnosisRequest)
        {

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, DIAGNONSIS_ENDPOINT);
            requestMessage.Headers.Add("App-Id", credentials.App_Id);
            requestMessage.Headers.Add("App-Key", credentials.App_Key);

            string json = JsonConvert.SerializeObject(diagnosisRequest);
            StringContent data = new StringContent(json, Encoding.UTF8, "application/json");

            requestMessage.Content = data;

            HttpClient client = clientFactory.CreateClient();
            HttpResponseMessage response = await client.SendAsync(requestMessage);

            DiagnonsisResponse diagnosisResponse = new DiagnonsisResponse();
            string contents = response.Content.ReadAsStringAsync().Result;
            if (response.IsSuccessStatusCode)
            {
                diagnosisResponse = JsonConvert.DeserializeObject<DiagnonsisResponse>(contents);
            }
            else
            {
                logger.LogError($"{response.StatusCode} - {response.Content}");
            }

            diagnosisResponse.StatusCode = response.StatusCode;

            return diagnosisResponse;
        }

        public async Task<TriageResponse> SendTriageRequestAsync(InfermedicaRequest triageRequest)
        {

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, TRIAGE_ENDPOINT);
            requestMessage.Headers.Add("App-Id", credentials.App_Id);
            requestMessage.Headers.Add("App-Key", credentials.App_Key);

            string json = JsonConvert.SerializeObject(triageRequest);
            StringContent data = new StringContent(json, Encoding.UTF8, "application/json");

            requestMessage.Content = data;

            using HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.SendAsync(requestMessage);

            TriageResponse triageResponse = new TriageResponse();
            string contents = response.Content.ReadAsStringAsync().Result;
            if (response.IsSuccessStatusCode)
            {
                triageResponse = JsonConvert.DeserializeObject<TriageResponse>(contents);
            }
            else
            {
                logger.LogError($"{response.StatusCode} - {response.Content}");
            }

            triageResponse.StatusCode = response.StatusCode;

            return triageResponse;
        }
    }
}
