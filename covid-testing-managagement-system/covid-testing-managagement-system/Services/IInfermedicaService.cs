﻿using covid_testing_managagement_system.Models.Infermedica.Request;
using covid_testing_managagement_system.Models.Infermedica.Response;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.Services
{
    public interface IInfermedicaService
    {
        Task<DiagnonsisResponse> SendDiagnosisRequestAsync(InfermedicaRequest diagnosisRequest);
        Task<TriageResponse> SendTriageRequestAsync(InfermedicaRequest triageRequest);
    }
}