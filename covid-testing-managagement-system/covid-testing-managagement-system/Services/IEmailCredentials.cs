namespace covid_testing_managagement_system.Services
{
    public interface IEmailCredentials
    {
        string MailServer { get; set; }
        int MailPort { get; set; }
        string SenderName { get; set; }
        string SenderAddress { get; set; }
        string Password { get; set; }
    }
}