using System.ComponentModel.DataAnnotations;

namespace covid_testing_managagement_system.Models
{
    public enum Speciality
    {
        GP,
        Physician,
        Nurse,
        Assistant,
        Admin,
        Aide,
        Caretaker,
        Technician,
        Pharmacist,
        Hygienist,
        Therapist,
        Pediatrician,
        Surgeon,
    }

    public enum Country
    {
        ZA,
        US,
        FR,
        UK,
        NZ,
        AU,
        EG,
        IL,
        BR
    }

    public class Professional : PersonalInfo
    {
        [Required]
        [StringLength(50)]
        public string RegistrationNumber { get; set; }
        public Country Country { get; set; }
        public Speciality Speciality { get; set; }
        public bool Reviewed { get; set; }
    }
}
