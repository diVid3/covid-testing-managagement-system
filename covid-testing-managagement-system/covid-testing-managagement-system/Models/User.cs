using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace covid_testing_managagement_system.Models
{
    public class User
    {
        public int Id { get; set; }
        public string NormalizedEmail { get; set; }
        public string PasswordHash { get; set; }
        public bool EmailConfirmed { get; set; }

        // Workaround to create other entities in the same transaction
        // as transactions are only supported on the same connection
        // An alternative is to add the Professional as an attribute here
        // and create it in UserStore.CreateAsync, but this solution uses
        // lambdas which are way cooler ;) 
        private List<Action<SqlConnection>> _commandList = new List<Action<SqlConnection>>();
        public void AddCommand(Action<SqlConnection> command)
        {
            this._commandList.Add(command);
        }

        public List<Action<SqlConnection>> GetCommands()
        {
            return this._commandList;
        }

        public void ExecuteCommands(SqlConnection connection)
        {
            foreach (var command in this._commandList)
            {
                command(connection);
            }
        }
    }
}
