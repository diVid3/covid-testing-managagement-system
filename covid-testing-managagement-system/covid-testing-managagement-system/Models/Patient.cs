using System;

namespace covid_testing_managagement_system.Models
{
    public class Patient : PersonalInfo
    {
        public DateTime DateApplied { get; set; }
        public int Age { get; set; }
        public Boolean Sex { get; set; }
    }
}
