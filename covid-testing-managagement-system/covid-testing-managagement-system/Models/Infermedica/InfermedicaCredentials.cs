﻿using Microsoft.Extensions.Configuration;

namespace covid_testing_managagement_system.Models.Infermedica
{
    public class InfermedicaCredentials : IInfermedicaCredentials
    {

        public InfermedicaCredentials(IConfiguration configuration)
        {
            configuration.GetSection("InfermedicaCredentials").Bind(this);
        }

        public string App_Id { get; set; }
        public string App_Key { get; set; }

    }
}
