﻿namespace covid_testing_managagement_system.Models.Infermedica
{
    public interface IInfermedicaCredentials
    {
        string App_Id { get; set; }
        string App_Key { get; set; }
    }
}