﻿using Newtonsoft.Json;
using System.Net;

namespace covid_testing_managagement_system.Models.Infermedica.Response
{
    public class DiagnonsisResponse
    {
        [JsonIgnore]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("question")]
        public Question Question { get; set; }
        [JsonProperty("should_stop")]
        public bool ShouldStop { get; set; }

        public DiagnonsisResponse()
        {

        }
    }
}
