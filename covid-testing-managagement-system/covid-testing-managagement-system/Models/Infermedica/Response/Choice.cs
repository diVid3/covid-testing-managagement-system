﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace covid_testing_managagement_system.Models.Infermedica.Response
{
    public class Choice
    {

        public enum ChoiceLabel
        {
            Yes,
            No
        }

        [JsonProperty("id")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Infermedica.Choice Id { get; set; }

        [JsonProperty("label")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ChoiceLabel Label { get; set; }
    }
}
