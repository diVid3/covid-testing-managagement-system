﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace covid_testing_managagement_system.Models.Infermedica.Response
{
    public class Question
    {
        public enum QuestionType
        {
            single,
            group_single,
            group_multiple
        }

        [JsonProperty("explanation")]
        public string Explanation { get; set; }


        [JsonProperty("items")]
        public List<Item> Items { get; set; }


        [JsonProperty("text")]
        public string Text { get; set; }


        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public QuestionType Type { get; set; }

        public Question()
        {
            Items = new List<Item>();
        }
    }
}
