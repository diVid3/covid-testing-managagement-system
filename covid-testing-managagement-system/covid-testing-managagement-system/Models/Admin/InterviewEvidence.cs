namespace covid_testing_managagement_system.Models.Admin
{
    public class InterviewEvidence
    {
        public int InterviewId { get; set; }
        public bool Choice { get; set; }
        public string ApiId { get; set; }
        public int Id { get; set; }
        public int EvidenceId { get; set; }
        public string Name { get; set; }
        public string CommonName { get; set; }
    }
}
