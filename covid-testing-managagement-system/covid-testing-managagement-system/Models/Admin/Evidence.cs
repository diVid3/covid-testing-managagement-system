﻿namespace covid_testing_managagement_system.Models.Admin
{
    public class Evidence
    {
        public int EvidenceId { get; set; }
        public string Name { get; set; }
        public string CommonName { get; set; }
        public string ApiId { get; set; }
    }
}
