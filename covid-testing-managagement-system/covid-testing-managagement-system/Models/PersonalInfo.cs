using System.ComponentModel.DataAnnotations;

namespace covid_testing_managagement_system.Models
{
    public class PersonalInfo
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        private string _email;
        public string Email
        {
            get
            {
                return this._email;
            }
            set
            {
                this.NormalizedEmail = value.ToUpperInvariant();
                this._email = value;
            }
        }
        public string NormalizedEmail { get; private set; }
        public string IdNumber { get; set; }
    }
}
